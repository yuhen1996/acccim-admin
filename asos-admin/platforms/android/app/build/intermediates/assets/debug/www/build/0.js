webpackJsonp([0],{

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SponsorDetailsPageModule", function() { return SponsorDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sponsordetails__ = __webpack_require__(362);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SponsorDetailsPageModule = /** @class */ (function () {
    function SponsorDetailsPageModule() {
    }
    SponsorDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sponsordetails__["a" /* SponsorDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__sponsordetails__["a" /* SponsorDetailsPage */]),
            ],
        })
    ], SponsorDetailsPageModule);
    return SponsorDetailsPageModule;
}());

//# sourceMappingURL=sponsordetails.module.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SponsorDetailsPage; });
/* unused harmony export Product */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SponsorDetailsPage = /** @class */ (function () {
    function SponsorDetailsPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.description = '';
        this.price = 0;
        this.photos = [];
        this.otherProducts = [];
        this.isMenSelected = navParams.get('isMenSelected');
        if (this.isMenSelected) {
            this.initialiseManProductDetails();
        }
        else {
            this.initialiseWomanProductDetails();
        }
    }
    SponsorDetailsPage.prototype.initialiseManProductDetails = function () {
        this.description = 'Lee Sherpa Rider Denim Jacket Mid Wash';
        this.price = 218;
        this.photos.push('../assets/images/Profile_dominic2.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-2.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-3.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-4.jpg');
        this.otherProducts.push(new Product('Pull&Bear Denim Jacket In Black', '../assets/images/Profile_ryan1.jpg', 40, 30));
        this.otherProducts.push(new Product('Liquor N Poker Oversized Denim Jacket Stonewas', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-2.jpg', 89, 79));
        this.otherProducts.push(new Product('Puma T7 Vintage Track Jacket In White 57498506', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-3.jpg', 110, 99));
        this.otherProducts.push(new Product('New Look Cotton Bomber Jacket In Burgundy', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-8.jpg', 50, 35));
    };
    SponsorDetailsPage.prototype.initialiseWomanProductDetails = function () {
        this.description = 'ASOS Cotton Mini Shirt Dress';
        this.price = 40;
        this.photos.push('../assets/images/Profile_wangyuhen2.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-2-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-3-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-4-women.jpg');
        this.otherProducts.push(new Product('Stradivarius Polka Dot Shirt Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-2', 40, 30));
        this.otherProducts.push(new Product('ASOS Ultimate Rolled Sleeve T-Shirt Dress With Tab', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-3', 30, 25));
        this.otherProducts.push(new Product('Boohoo One Shoulder Floral Midi Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-4', 40, 30));
        this.otherProducts.push(new Product('Boohoo Off Shoulder Lemon Print Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-5', 44, 40));
    };
    SponsorDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductDetailsPage');
    };
    SponsorDetailsPage.prototype.sizePopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    SponsorDetailsPage.prototype.likeUnlike = function () {
        this.isLiked = !this.isLiked;
    };
    SponsorDetailsPage.prototype.goToHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    SponsorDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sponsordetails',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM App\asos-admin\src\pages\sponsordetails\sponsordetails.html"*/'<!--\n\n<ion-header>\n\n        <ion-buttons left>\n\n                <button ion-button icon-only (click)="backHistory()" >\n\n                    <ion-icon name="arrow-back"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n    </ion-header>\n\n  -->\n\n  <ion-header no-border>\n\n    <ion-navbar transparent>\n\n      <ion-buttons right>\n\n        <button class="navbar-button" ion-button icon-only>\n\n          <!--\n\n          <ion-icon name="md-share"></ion-icon>\n\n          -->\n\n        </button>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n  </ion-header>\n\n\n\n    <ion-content>\n\n        <br> \n\n\n\n        <ion-card>\n\n\n\n          <img src="{{ photos[0] }}">\n\n              <ion-fab right top>\n\n               \n\n              </ion-fab>\n\n            \n\n                  \n\n            \n\n            </ion-card>\n\n            \n\n           \n\n                  \n\n    </ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM App\asos-admin\src\pages\sponsordetails\sponsordetails.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], SponsorDetailsPage);
    return SponsorDetailsPage;
}());

var Product = /** @class */ (function () {
    function Product(name, picture, price, discountPrice) {
        this.name = name;
        this.picture = picture;
        this.price = price;
        this.discountPrice = discountPrice;
    }
    return Product;
}());

//# sourceMappingURL=sponsordetails.js.map

/***/ })

});
//# sourceMappingURL=0.js.map