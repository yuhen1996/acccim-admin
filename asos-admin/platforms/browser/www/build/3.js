webpackJsonp([3],{

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsPageModule", function() { return ProductDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_details__ = __webpack_require__(359);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProductDetailsPageModule = /** @class */ (function () {
    function ProductDetailsPageModule() {
    }
    ProductDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetailsPage */]),
            ],
        })
    ], ProductDetailsPageModule);
    return ProductDetailsPageModule;
}());

//# sourceMappingURL=product-details.module.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailsPage; });
/* unused harmony export Product */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_size_pop_over_product_size_pop_over__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductDetailsPage = /** @class */ (function () {
    function ProductDetailsPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.description = '';
        this.price = 0;
        this.photos = [];
        this.otherProducts = [];
        this.isMenSelected = navParams.get('isMenSelected');
        if (this.isMenSelected) {
            this.initialiseManProductDetails();
        }
        else {
            this.initialiseWomanProductDetails();
        }
    }
    ProductDetailsPage.prototype.initialiseManProductDetails = function () {
        this.description = 'Lee Sherpa Rider Denim Jacket Mid Wash';
        this.price = 218;
        this.photos.push('../assets/images/Profile_ryan1.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-2.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-3.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-4.jpg');
        this.otherProducts.push(new Product('Pull&Bear Denim Jacket In Black', '../assets/images/Profile_ryan1.jpg', 40, 30));
        this.otherProducts.push(new Product('Liquor N Poker Oversized Denim Jacket Stonewas', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-2.jpg', 89, 79));
        this.otherProducts.push(new Product('Puma T7 Vintage Track Jacket In White 57498506', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-3.jpg', 110, 99));
        this.otherProducts.push(new Product('New Look Cotton Bomber Jacket In Burgundy', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-8.jpg', 50, 35));
    };
    ProductDetailsPage.prototype.initialiseWomanProductDetails = function () {
        this.description = 'ASOS Cotton Mini Shirt Dress';
        this.price = 40;
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-women-1.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-2-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-3-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-4-women.jpg');
        this.otherProducts.push(new Product('Stradivarius Polka Dot Shirt Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-2', 40, 30));
        this.otherProducts.push(new Product('ASOS Ultimate Rolled Sleeve T-Shirt Dress With Tab', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-3', 30, 25));
        this.otherProducts.push(new Product('Boohoo One Shoulder Floral Midi Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-4', 40, 30));
        this.otherProducts.push(new Product('Boohoo Off Shoulder Lemon Print Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-5', 44, 40));
    };
    ProductDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductDetailsPage');
    };
    ProductDetailsPage.prototype.sizePopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    ProductDetailsPage.prototype.likeUnlike = function () {
        this.isLiked = !this.isLiked;
    };
    ProductDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-product-details',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM App\asos-admin\src\pages\product-details\product-details.html"*/'<ion-header no-border>\n  <ion-navbar transparent>\n    <ion-buttons right>\n      <button class="navbar-button" ion-button icon-only>\n        <ion-icon name="md-share"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content fullscreen padding>\n  <ion-slides pager>\n    <ion-slide>\n      <img src="{{ photos[0] }}">\n    </ion-slide>\n\n    <ion-slide>\n      <img src="{{ photos[1] }}">\n    </ion-slide>\n\n    <ion-slide>\n      <img src="{{ photos[2] }}">\n    </ion-slide>\n\n    <ion-slide>\n      <img src="{{ photos[3] }}">\n    </ion-slide>\n  </ion-slides>\n\n  <ion-grid id="product-details">\n    <ion-row>\n      <ion-col text-center>\n        <button class="play-video" ion-button clear icon-only>\n          <ion-icon name="ios-play-outline"></ion-icon>\n          <p>VIDEO</p>\n        </button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-item>\n        <p class="product-description">{{ description }}</p>\n        <p class="product-price">${{ price }}.00</p>\n      </ion-item>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <p class="product-count">100</p>\n      </ion-col>\n\n      <ion-col>\n        <ion-item class="product-size" (click)="sizePopOver($event)">\n          <p item-start>SIZE</p>\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col col-10>\n        <button class="add-to-bag" ion-button full icon-only>\n          <p>ADD TO BAG</p>\n        </button>\n      </ion-col>\n\n      <ion-col col-2>\n        <button class="product-like" item-end ion-button clear icon-only (click)="likeUnlike()">\n          <ion-icon *ngIf="isLiked" name="ios-heart"></ion-icon>\n          <ion-icon *ngIf="!isLiked" name="ios-heart-outline"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <p class="product-external-links">FREE SHIPPING AND RETURNS</p>\n    </ion-row>\n\n    <ion-row>\n      <p class="product-external-links">PRODUCT DETAILS</p>\n    </ion-row>\n\n    <ion-row>\n      <p class="product-external-links">SIZE GUIDE</p>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid id="related-products">\n    <ion-item class="buy-look-row">\n      <p class="buy-the-look" item-start>BUY THE LOOK</p>\n      <p item-end>{{ otherProducts.length }} items</p>\n    </ion-item>\n\n    <ion-scroll scrollX="true" scroll-avatar>\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[0].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[0].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[0].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[0].name }}</p>\n      </ion-col>\n\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[1].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[1].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[1].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[1].name }}</p>\n      </ion-col>\n\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[2].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[2].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[2].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[2].name }}</p>\n      </ion-col>\n\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[3].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[3].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[3].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[3].name }}</p>\n      </ion-col>\n    </ion-scroll>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM App\asos-admin\src\pages\product-details\product-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], ProductDetailsPage);
    return ProductDetailsPage;
}());

var Product = /** @class */ (function () {
    function Product(name, picture, price, discountPrice) {
        this.name = name;
        this.picture = picture;
        this.price = price;
        this.discountPrice = discountPrice;
    }
    return Product;
}());

//# sourceMappingURL=product-details.js.map

/***/ })

});
//# sourceMappingURL=3.js.map