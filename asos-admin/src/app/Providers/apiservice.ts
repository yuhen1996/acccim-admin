import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError,tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Http, Response } from '@angular/http';


/*
  Generated class for the ListingServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
//Declare a httpOptions and mention the header in json format so that HTTP Post method can use this option.
const httpOptions = {
  headers : new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable()
export class ApiserviceProvider {
  data: Observable<any>;
  data1: any;
  constructor(
    public http: HttpClient, 
    public https: Http) {
  
  }

  public AdminLoginAPI = "https://api.acccim-registration.org.my/api/AdminSignIn?id=";
  public eventListing = "https://api.acccim-registration.org.my/api/EventListing";
  public specificevent = "https://api.acccim-registration.org.my/api/SpecificEvent?eventid=";
  public eventguest = "https://api.acccim-registration.org.my/api/Guest";
  public eventmember = "https://api.acccim-registration.org.my/api/Member";
  public eventnonmember = "https://api.acccim-registration.org.my/api/NonMember";


  //User Login
  adminLogin(user,pass)
  {
    var id = user;
    var pw = pass;

    if(id != null && pw != null)
    {
      return this.https.get(this.AdminLoginAPI + id + '&&pw=' + pw);
    }
  }

  DisplayAdmin(user,pass)
  {
    var id = user;
    var pw = pass;

      return this.https.get(this.AdminLoginAPI + id + '&&pw=' + pw);
    
  }

  getEventListing()
  {
    return this.https.get(this.eventListing);
  }
  
  getEvent()
  {
    return this.https.get(this.eventListing);
  }
  
  getSpecificEvent(EventID)
  {
    var eventid = EventID;
    
    return this.https.get(this.specificevent + eventid);
  }
  getEventGuest()
  {
    return this.https.get(this.eventguest);
  }
  getEventMember()
  {
    return this.https.get(this.eventmember);
  }
  getEventNonmember()
  {
    return this.https.get(this.eventnonmember);
  }

}
