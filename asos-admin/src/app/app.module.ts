import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MenWomenCategoryPopOverPage } from '../pages/men-women-category-pop-over/men-women-category-pop-over';
import { BagPage } from '../pages/bag/bag';
import { SavedItemsPage } from '../pages/saved-items/saved-items';
import { AccountPage } from '../pages/account/account';
import { AppSettingsPage } from '../pages/app-settings/app-settings';
import { HelpFaqPage } from '../pages/help-faq/help-faq';
import { SavedItemsPageModule } from '../pages/saved-items/saved-items.module';
import { RecommendedPopOverPage } from '../pages/recommended-pop-over/recommended-pop-over';
import { FilterPage } from '../pages/filter/filter';
import { ProductSizePopOverPage } from '../pages/product-size-pop-over/product-size-pop-over';
import { ProductColourPopOverPage } from '../pages/product-colour-pop-over/product-colour-pop-over';

import { PopupModalPage } from '../pages/popupmodal/popupmodal';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { SponsorDetailsPage } from '../pages/sponsordetails/sponsordetails';
import { ProgrammePage} from '../pages/programme/programme';

import { IonAlphaScrollModule } from 'ionic4-alpha-scroll';
import {NgxQRCodeModule} from 'ngx-qrcode2';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ApiserviceProvider } from '../app/Providers/apiservice';
import { ProgrammePageModule } from '../pages/programme/programme.module';
 
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MenWomenCategoryPopOverPage,
    RecommendedPopOverPage,
    ProductSizePopOverPage,
    ProductColourPopOverPage,
    BagPage,
    AccountPage,
    AppSettingsPage,
    HelpFaqPage,
    FilterPage,
    PopupModalPage,
   
    LoginPage,
    RegisterPage,
    
   
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SavedItemsPageModule,
    HttpClientModule,
    IonAlphaScrollModule,
    ProgrammePageModule,
    NgxQRCodeModule,
    HttpModule
  ],

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MenWomenCategoryPopOverPage,
    RecommendedPopOverPage,
    ProductSizePopOverPage,
    ProductColourPopOverPage,
    BagPage,
    SavedItemsPage,
    AccountPage,
    AppSettingsPage,
    HelpFaqPage,
    FilterPage,
    PopupModalPage,
    ProgrammePage,
    LoginPage,
    RegisterPage,
    
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiserviceProvider,
    BarcodeScanner
  ]
})
export class AppModule {}
