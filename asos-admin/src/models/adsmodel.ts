import { DateTime } from "ionic-angular";

//Declare all the value in this model can so that other page can using this model class to add or edit data from the api.
export class AdsModel {
    AdsID: string;
    AdsImage: string;
    AdsTitle: string;
    AdsDesciption1: string;
    AdsDesciption2: string;
    AdsDesciption3: string;
    AdsType: string;
    AddedDate:  string = new Date().toLocaleDateString();
    PostedBy: string;
    SponsorType: string;
    SponsorTypeExclusive: string;
    Souvenir: string;
    StageAcknowledgement: string;
    ConferencePrintedLogo: any;
    AdvertisementAirtimeConference: string;
    SlideRotatingOnVideoScreen: string;
    ComplimentarySeatYEC: string;
    ComplimentarySeatWelcomingDinner: string;
    BuntingDisplay: string;
    ExhibitSpace: string;
    SponsorMention: string;
    LogoAndCompanyProfileWeb: string;
    LogoAndCompanyProfileApp: string;
    AdsDisplayBannerApp: string;
    SocialMediaMentionYEC: string;
    PrintedLogoEventBackdrop: string;
    InterviewVideo: string;
    FeaturedArticleConference: string;
    SponsorTypeValue: string;
    SponsorCompany: string;
    SponsorTel: string;
    PurchaseDate: string = new Date().toLocaleDateString();
    SponsorProfile: string;
    SponsorBackground: string;
    /*CompletionDate: string = new Date().toLocaleDateString();
      AddedDate: DateTime;
      SponsorBackground: Float32Array; */
}