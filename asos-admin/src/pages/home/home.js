angular.module('Mob').controller('TeamCtrl', function($scope, $ionicModal) {

    /* Modal */
    $ionicModal.fromTemplateUrl('homes.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });
    $scope.openModal = function() {
      $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
  
    // opens the modal only one time
    if(!localStorage.getItem("popupWasShown")){
      $scope.modal.show();
      localStorage.setItem("popupWasShown", true);
    }
  
  });

  