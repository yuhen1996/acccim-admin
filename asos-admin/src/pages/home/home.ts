import { Component,ViewChild } from '@angular/core';
import { NavController, PopoverController, Events, ModalController,NavParams } from 'ionic-angular';
import { MenWomenCategoryPopOverPage } from '../men-women-category-pop-over/men-women-category-pop-over';
import { CategoryPage, Category } from '../category/category';
import { Slides } from 'ionic-angular';
import { SponsorDetailsPage } from '../sponsordetails/sponsordetails';
import { Observable } from 'rxjs/Observable';
import { ApiserviceProvider } from '../../app/Providers/apiservice';
import {Http,Response} from '@angular/http';
import { ProgrammePage } from '../programme/programme';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  homeSegment: string = "home";
  isMenSelected: boolean = true;
  pageTitle: string = 'MEN';
  
  user: string;
  pass: string;
  showuser: any;
  showEvent: any;
  showMember: any;
  showEventMember: any;
  showEventNonmember: any;
  showEventGuest: any;
  

  public event = {};
  
  images = ['../../assets/images/Profile_wangyuhen2.jpg', '../../assets/images/Profile_dominic2.jpg', '../../assets/images/Profile_zainul1.jpg', '../../assets/images/Profile_ryan1.jpg'];
  d_images = ['../../assets/images/a_diamond.png'];
  p_images = ['../../assets/images/a_platinum.png', '../../assets/images/a_platinum.png', '../../assets/images/a_platinum.png'];
  g_images = ['../../assets/images/a_gold.png', '../../assets/images/a_gold.png', '../../assets/images/a_gold.png', '../../assets/images/a_gold.png', '../../assets/images/a_gold.png'];
  s1_images = ['../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png'];
  s2_images = ['../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png'];
  s3_images = ['../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png'];

  
  visitorview: any = "member";

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,
    private modal:ModalController, public events: Events, navParams: NavParams,public ApiService: ApiserviceProvider) {
    events.subscribe('genderShoppingChanged', (isMenSelected) => {
      this.isMenSelected = isMenSelected;
      this.pageTitle = this.isMenSelected ? 'MEN' : 'WOMEN';

     
    });

    this.user = navParams.get('data');
    this.pass = navParams.get('data1');
    this.loadAdminDetails();
    this.loadEvent();
    this.loadEventMember();
    this.loadEventNonmember();
    this.loadEventGuest();
 
  }
  loadAdminDetails()
  {
    this.ApiService.adminLogin(this.user,this.pass)
    .subscribe((res:Response) => {
      const getUser = res.json();
      this.showuser = getUser;
      
    })
  }

  loadEvent()
  {
    this.ApiService.getEvent()
    .subscribe((res:Response) => {
      const getEvent = res.json();
      this.showEvent = getEvent;

      //console.log(this.showEvent.MemberAvailability_tb.MemberType);
    });
  }


  loadEventMember()
  {
    this.ApiService.getEventMember()
    .subscribe((res:Response) => {
      const getEventMember = res.json();
      this.showEventMember = getEventMember;
    });
  }
  
  loadEventNonmember()
  {
    this.ApiService.getEventNonmember()
    .subscribe((res:Response) => {
      const getEventNonmember = res.json();
      this.showEventNonmember = getEventNonmember;
    });
  }
  
  loadEventGuest()
  {
    this.ApiService.getEventGuest()
    .subscribe((res:Response) => {
      const getEventGuest = res.json();
      this.showEventGuest = getEventGuest;
    });
  }

  
  

  //Move to Next slide
  slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }
 
  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }
 
  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }
 
  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }
 
  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }
 



  

  showPopOver(myEvent) {
    let popover = this.popoverCtrl.create(MenWomenCategoryPopOverPage);

    popover.present({
      ev: myEvent
    });
  }

 

  goToCategory(category: string) {
    var navParams = { 
      category: category, 
      isMenSelected: this.isMenSelected
    };

    this.navCtrl.push('CategoryPage', navParams);
  }

  goToProducts() {
    var navParams = { 
      category: 'CLOTHING', 
      isMenSelected: this.isMenSelected
    };

    this.navCtrl.push('ProductDetailsPage', navParams);
  }

  changeTab(value){
    this.visitorview =value;
}


goToSponsorDetails() {
  var navParams = { 
    isMenSelected: this.isMenSelected
  };

  this.navCtrl.push('SponsorDetailsPage', navParams);
 
}

goToQR() {

  this.navCtrl.push('QRcodePage');
}

gotoProgramme(){
  this.navCtrl.push('ProgrammePage');
}

goToSavedItems() {
  this.navCtrl.push('SavedItemsPage');
}

}
