import {Component} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController} from "ionic-angular";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import {PopupModalPage} from "../popupmodal/popupmodal";
import { ApiserviceProvider } from '../../app/Providers/apiservice';
import {Http,Response} from '@angular/http';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  user:any;
  pass:any;
  constructor(public nav: NavController, public forgotCtrl: AlertController, 
    public menu: MenuController, public toastCtrl: ToastController,
    public ApiService: ApiserviceProvider) {
    this.menu.swipeEnable(false);
  }

  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  // login and go to home page
  login() {
  this.ApiService.adminLogin(this.user, this.pass).subscribe(
    (res:Response) => {
      const checkLogin = res.json();
      
      let toast = this.toastCtrl.create({
        message: 'Successfully Login',
        duration: 2000,
        position: 'top',
        cssClass: 'dark-trans',
        closeButtonText: 'OK',
        showCloseButton: true
      });
      toast.present();
      
        this.nav.setRoot(HomePage,{
          data: this.user,
          data1: this.pass
        });
        
   
    })
    
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
