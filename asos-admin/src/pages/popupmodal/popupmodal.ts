import {Component} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController} from "ionic-angular";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-popupmodal',
  templateUrl: 'popupmodal.html',
})

export class PopupModalPage {

  constructor(public nav: NavController, ) {
    
  }

  
  goToHome() {
    this.nav.setRoot(HomePage);
  }
}
