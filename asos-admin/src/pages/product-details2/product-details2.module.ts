import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductDetails2Page } from './product-details2';

@NgModule({
  declarations: [
    ProductDetails2Page,
  ],
  imports: [
    IonicPageModule.forChild(ProductDetails2Page),
  ],
})
export class ProductDetailsPageModule {}