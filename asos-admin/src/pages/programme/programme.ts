import {Component} from "@angular/core";
import { IonicPage, NavController, NavParams, PopoverController, Events } from 'ionic-angular';
import {HomePage} from "../home/home";
import { IonicPageModule } from 'ionic-angular';
import { ApiserviceProvider } from '../../app/Providers/apiservice';
import {Http,Response} from '@angular/http';

import { BarcodeScanner} from '@ionic-native/barcode-scanner';
import { HttpClient } from "@angular/common/http";
//import { BarcodeScanner } from "@ionic-native/barcode-scanner/index";



@IonicPage()
@Component({
  selector: 'page-programme',
  templateUrl: 'programme.html',
})

export class ProgrammePage {


  qrData = null;
  createdCode = null;
  scannedCode = null;
  
  homeSegment: string = "Details";

  user: string;
  pass: string;
  showuser: any;
  showEvent: any;
  showMember: any;
  showSpecEvent: any;
  showEventMember: any;
  showEventNonmember: any;
  showEventGuest: any;
  EventID : any;

  public event = {};
 

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public events: Events,
    public httpClient: HttpClient, public https:Http,
    public ApiService: ApiserviceProvider,
    public popoverCtrl: PopoverController,
    public barcodeScanner: BarcodeScanner) {
   

      this.EventID = navParams.get('data');
      this.loadEvent();
      this.loadSpecificEvent();
      this.loadEventMember();
      this.loadEventNonmember();
      this.loadEventGuest();
      
  }

  

  goToHome() {
    this.navCtrl.setRoot(HomePage);
  }

  scanCode(){
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    })
}

  loadEvent()
  {
    this.ApiService.getEvent()
    .subscribe((res:Response) => {
      const getEvent = res.json();
      this.showEvent = getEvent;
    });
  }

  loadSpecificEvent()
  {
    this.ApiService.getSpecificEvent(this.EventID)
    .subscribe((res:Response) => {
      const getSpecificEvent = res.json();
      this.showSpecEvent = getSpecificEvent.eventid;
    });
  }

  loadEventMember()
  {
    this.ApiService.getEventMember()
    .subscribe((res:Response) => {
      const getEventMember = res.json();
      this.showEventMember = getEventMember;
    });
  }
  
  loadEventNonmember()
  {
    this.ApiService.getEventNonmember()
    .subscribe((res:Response) => {
      const getEventNonmember = res.json();
      this.showEventNonmember = getEventNonmember;
    });
  }
  
  loadEventGuest()
  {
    this.ApiService.getEventGuest()
    .subscribe((res:Response) => {
      const getEventGuest = res.json();
      this.showEventGuest = getEventGuest;
    });
  }
}