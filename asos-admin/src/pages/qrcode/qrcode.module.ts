import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QRcodePage } from './qrcode';
import {NgxQRCodeModule} from 'ngx-qrcode2';

@NgModule({
  declarations: [
    QRcodePage,
  ],
  imports: [
    NgxQRCodeModule,
    IonicPageModule.forChild(QRcodePage),
  ],
})
export class QRcodePageModule {}