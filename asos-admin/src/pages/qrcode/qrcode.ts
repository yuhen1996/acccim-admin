import {Component} from "@angular/core";
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import {HomePage} from "../home/home";

import { BarcodeScanner} from '@ionic-native/barcode-scanner';
//import { BarcodeScanner } from "@ionic-native/barcode-scanner/index";



@IonicPage()
@Component({
  selector: 'page-qrcode',
  templateUrl: 'qrcode.html',
})

export class QRcodePage {
qrData = null;
createdCode = null;
scannedCode = null;
 

  constructor(public barcodeScanner: BarcodeScanner, public navCtrl: NavController, public navParams: NavParams, 
    public popoverCtrl: PopoverController) {
   
  }

  createCode(){
      this.createdCode = this.qrData;
  }

  scanCode(){
      this.barcodeScanner.scan().then(barcodeData => {
        this.scannedCode = barcodeData.text;
      })
  }

  
  goToHome() {
    this.navCtrl.setRoot(HomePage);
  }
}

