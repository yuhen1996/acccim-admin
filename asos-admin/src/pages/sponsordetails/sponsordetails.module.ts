import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SponsorDetailsPage } from './sponsordetails';

@NgModule({
  declarations: [
    SponsorDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SponsorDetailsPage),
  ],
})
export class SponsorDetailsPageModule {}