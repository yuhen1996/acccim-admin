webpackJsonp([4],{

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsPageModule", function() { return ProductDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_details2__ = __webpack_require__(361);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProductDetailsPageModule = /** @class */ (function () {
    function ProductDetailsPageModule() {
    }
    ProductDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__product_details2__["a" /* ProductDetails2Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__product_details2__["a" /* ProductDetails2Page */]),
            ],
        })
    ], ProductDetailsPageModule);
    return ProductDetailsPageModule;
}());

//# sourceMappingURL=product-details2.module.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetails2Page; });
/* unused harmony export Product */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_size_pop_over_product_size_pop_over__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductDetails2Page = /** @class */ (function () {
    function ProductDetails2Page(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.description = '';
        this.price = 0;
        this.photos = [];
        this.otherProducts = [];
        this.isMenSelected = navParams.get('isMenSelected');
        if (this.isMenSelected) {
            this.initialiseManProductDetails();
        }
        else {
            this.initialiseWomanProductDetails();
        }
    }
    ProductDetails2Page.prototype.initialiseManProductDetails = function () {
        this.description = 'Zainul the God of PHP and Backend';
        this.price = 218;
        this.photos.push('../../assets/images/Profile_zainul1.jpg');
        this.photos.push('../../assets/images/Profile_zainul1.jpg');
        this.photos.push('../../assets/images/Profile_zainul1.jpg');
        this.photos.push('../../assets/images/Profile_zainul1.jpg');
        this.otherProducts.push(new Product('Pull&Bear Denim Jacket In Black', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-1.jpg', 40, 30));
        this.otherProducts.push(new Product('Liquor N Poker Oversized Denim Jacket Stonewas', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-2.jpg', 89, 79));
        this.otherProducts.push(new Product('Puma T7 Vintage Track Jacket In White 57498506', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-3.jpg', 110, 99));
        this.otherProducts.push(new Product('New Look Cotton Bomber Jacket In Burgundy', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-8.jpg', 50, 35));
    };
    ProductDetails2Page.prototype.initialiseWomanProductDetails = function () {
        this.description = 'ASOS Cotton Mini Shirt Dress';
        this.price = 40;
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-women-1.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-2-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-3-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-4-women.jpg');
        this.otherProducts.push(new Product('Stradivarius Polka Dot Shirt Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-2', 40, 30));
        this.otherProducts.push(new Product('ASOS Ultimate Rolled Sleeve T-Shirt Dress With Tab', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-3', 30, 25));
        this.otherProducts.push(new Product('Boohoo One Shoulder Floral Midi Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-4', 40, 30));
        this.otherProducts.push(new Product('Boohoo Off Shoulder Lemon Print Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-5', 44, 40));
    };
    ProductDetails2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductDetails2Page');
    };
    ProductDetails2Page.prototype.sizePopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    ProductDetails2Page.prototype.likeUnlike = function () {
        this.isLiked = !this.isLiked;
    };
    ProductDetails2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-product-details2',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\product-details2\product-details2.html"*/'<ion-header no-border>\n\n        <ion-navbar transparent>\n\n          <ion-buttons right>\n\n            <button class="navbar-button" ion-button icon-only>\n\n              <ion-icon name="md-share"></ion-icon>\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-navbar>\n\n      </ion-header>\n\n      \n\n      <ion-content fullscreen padding>\n\n        <ion-slides pager>\n\n          <ion-slide>\n\n            <img src="{{ photos[0] }}">\n\n          </ion-slide>\n\n      \n\n          <ion-slide>\n\n            <img src="{{ photos[1] }}">\n\n          </ion-slide>\n\n      \n\n          <ion-slide>\n\n            <img src="{{ photos[2] }}">\n\n          </ion-slide>\n\n      \n\n          <ion-slide>\n\n            <img src="{{ photos[3] }}">\n\n          </ion-slide>\n\n        </ion-slides>\n\n      \n\n        <ion-grid id="product-details">\n\n          <ion-row>\n\n            <ion-col text-center>\n\n              <button class="play-video" ion-button clear icon-only>\n\n                <ion-icon name="ios-play-outline"></ion-icon>\n\n                <p>VIDEO</p>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n      \n\n          <ion-row>\n\n            <ion-item>\n\n              <p class="product-description">{{ description }}</p>\n\n              <p class="product-price">${{ price }}.00</p>\n\n            </ion-item>\n\n          </ion-row>\n\n      \n\n          <ion-row>\n\n            <ion-col>\n\n              <p class="product-count">100</p>\n\n            </ion-col>\n\n      \n\n            <ion-col>\n\n              <ion-item class="product-size" (click)="sizePopOver($event)">\n\n                <p item-start>SIZE</p>\n\n                <ion-icon item-end name="ios-arrow-down"></ion-icon>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n      \n\n          <ion-row>\n\n            <ion-col col-10>\n\n              <button class="add-to-bag" ion-button full icon-only>\n\n                <p>ADD TO BAG</p>\n\n              </button>\n\n            </ion-col>\n\n      \n\n            <ion-col col-2>\n\n              <button class="product-like" item-end ion-button clear icon-only (click)="likeUnlike()">\n\n                <ion-icon *ngIf="isLiked" name="ios-heart"></ion-icon>\n\n                <ion-icon *ngIf="!isLiked" name="ios-heart-outline"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n      \n\n          <ion-row>\n\n            <p class="product-external-links">FREE SHIPPING AND RETURNS</p>\n\n          </ion-row>\n\n      \n\n          <ion-row>\n\n            <p class="product-external-links">PRODUCT DETAILS</p>\n\n          </ion-row>\n\n      \n\n          <ion-row>\n\n            <p class="product-external-links">SIZE GUIDE</p>\n\n          </ion-row>\n\n        </ion-grid>\n\n      \n\n        <ion-grid id="related-products">\n\n          <ion-item class="buy-look-row">\n\n            <p class="buy-the-look" item-start>BUY THE LOOK</p>\n\n            <p item-end>{{ otherProducts.length }} items</p>\n\n          </ion-item>\n\n      \n\n          <ion-scroll scrollX="true" scroll-avatar>\n\n            <ion-col class="scroll-item" padding>\n\n              <img class="product-image" src="{{ otherProducts[0].picture }}">\n\n      \n\n              <ion-item no-lines>\n\n                <p class="discount-price" item-start>${{ otherProducts[0].discountPrice }}</p>\n\n      \n\n                <p class="product-price">${{ otherProducts[0].price }}</p>\n\n      \n\n                <button class="product-like" item-end ion-button clear icon-only>\n\n                  <ion-icon name="md-heart-outline"></ion-icon>\n\n                </button>\n\n              </ion-item>\n\n      \n\n              <p class="product-brand-description">{{ otherProducts[0].name }}</p>\n\n            </ion-col>\n\n      \n\n            <ion-col class="scroll-item" padding>\n\n              <img class="product-image" src="{{ otherProducts[1].picture }}">\n\n      \n\n              <ion-item no-lines>\n\n                <p class="discount-price" item-start>${{ otherProducts[1].discountPrice }}</p>\n\n      \n\n                <p class="product-price">${{ otherProducts[1].price }}</p>\n\n      \n\n                <button class="product-like" item-end ion-button clear icon-only>\n\n                  <ion-icon name="md-heart-outline"></ion-icon>\n\n                </button>\n\n              </ion-item>\n\n      \n\n              <p class="product-brand-description">{{ otherProducts[1].name }}</p>\n\n            </ion-col>\n\n      \n\n            <ion-col class="scroll-item" padding>\n\n              <img class="product-image" src="{{ otherProducts[2].picture }}">\n\n      \n\n              <ion-item no-lines>\n\n                <p class="discount-price" item-start>${{ otherProducts[2].discountPrice }}</p>\n\n      \n\n                <p class="product-price">${{ otherProducts[2].price }}</p>\n\n      \n\n                <button class="product-like" item-end ion-button clear icon-only>\n\n                  <ion-icon name="md-heart-outline"></ion-icon>\n\n                </button>\n\n              </ion-item>\n\n      \n\n              <p class="product-brand-description">{{ otherProducts[2].name }}</p>\n\n            </ion-col>\n\n      \n\n            <ion-col class="scroll-item" padding>\n\n              <img class="product-image" src="{{ otherProducts[3].picture }}">\n\n      \n\n              <ion-item no-lines>\n\n                <p class="discount-price" item-start>${{ otherProducts[3].discountPrice }}</p>\n\n      \n\n                <p class="product-price">${{ otherProducts[3].price }}</p>\n\n      \n\n                <button class="product-like" item-end ion-button clear icon-only>\n\n                  <ion-icon name="md-heart-outline"></ion-icon>\n\n                </button>\n\n              </ion-item>\n\n      \n\n              <p class="product-brand-description">{{ otherProducts[3].name }}</p>\n\n            </ion-col>\n\n          </ion-scroll>\n\n        </ion-grid>\n\n      </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\product-details2\product-details2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], ProductDetails2Page);
    return ProductDetails2Page;
}());

var Product = /** @class */ (function () {
    function Product(name, picture, price, discountPrice) {
        this.name = name;
        this.picture = picture;
        this.price = price;
        this.discountPrice = discountPrice;
    }
    return Product;
}());

//# sourceMappingURL=product-details2.js.map

/***/ })

});
//# sourceMappingURL=4.js.map