webpackJsonp([17],{

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_Providers_apiservice__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(nav, forgotCtrl, menu, toastCtrl, ApiService) {
        this.nav = nav;
        this.forgotCtrl = forgotCtrl;
        this.menu = menu;
        this.toastCtrl = toastCtrl;
        this.ApiService = ApiService;
        this.menu.swipeEnable(false);
    }
    // go to register page
    LoginPage.prototype.register = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    // login and go to home page
    LoginPage.prototype.login = function () {
        var _this = this;
        this.ApiService.adminLogin(this.user, this.pass).subscribe(function (res) {
            var checkLogin = res.json();
            var toast = _this.toastCtrl.create({
                message: 'Successfully Login',
                duration: 2000,
                position: 'top',
                cssClass: 'dark-trans',
                closeButtonText: 'OK',
                showCloseButton: true
            });
            toast.present();
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], {
                data: _this.user,
                data1: _this.pass
            });
        });
    };
    LoginPage.prototype.forgotPass = function () {
        var _this = this;
        var forgot = this.forgotCtrl.create({
            title: 'Forgot Password?',
            message: "Enter you email address to send a reset link password.",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Email',
                    type: 'email'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        console.log('Send clicked');
                        var toast = _this.toastCtrl.create({
                            message: 'Email was sended successfully',
                            duration: 3000,
                            position: 'top',
                            cssClass: 'dark-trans',
                            closeButtonText: 'OK',
                            showCloseButton: true
                        });
                        toast.present();
                    }
                }
            ]
        });
        forgot.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\login\login.html"*/'<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!-- -->\n\n<ion-content padding class="animated fadeIn login auth-page">\n\n  <div class="login-content">\n\n\n\n    <!-- Logo -->\n\n    <div padding-horizontal text-center class="animated fadeInDown">\n\n      <div class="logo">\n\n        <ion-img style="width: 100px; height: 100px;" src="../../assets/images/acccim_logo.gif"></ion-img>\n\n      </div>\n\n      \n\n    </div>\n\n\n\n    <!-- Login form -->\n\n    <form class="list-form">\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="mail" item-start class="text-primary"></ion-icon>\n\n          ID / Email\n\n        </ion-label>\n\n        <ion-input type="email" [(ngModel)]="user" name="user"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n          Password\n\n        </ion-label>\n\n        <ion-input type="password" [(ngModel)]="pass" name="pass"></ion-input>\n\n      </ion-item>\n\n    </form>\n\n\n\n    <p text-right ion-text color="secondary" tappable (click)="forgotPass()"><strong>Forgot Password?</strong></p>\n\n\n\n    <div>\n\n\n\n        <ion-grid>\n\n            <ion-row>\n\n                <ion-col col-4>\n\n                </ion-col>\n\n\n\n              <ion-col col-4>\n\n      <button ion-button icon-start block color="orange" tappable (click)="login()">\n\n        <ion-icon name="log-in"></ion-icon>\n\n        SIGN IN\n\n      </button>\n\n\n\n      </ion-col>\n\n\n\n\n\n      <ion-col col-4>\n\n        </ion-col>\n\n\n\n      </ion-row>\n\n      </ion-grid>\n\n\n\n     \n\n    </div>\n\n\n\n\n\n   \n\n\n\n  </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__app_Providers_apiservice__["a" /* ApiserviceProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductSizePopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductSizePopOverPage = /** @class */ (function () {
    function ProductSizePopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    ProductSizePopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductSizePopOverPage');
    };
    ProductSizePopOverPage.prototype.closePopover = function () {
        this.viewController.dismiss();
    };
    ProductSizePopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-product-size-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\product-size-pop-over\product-size-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      XS\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      S\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      M\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button class="out-of-stock" ion-item no-lines (click)="closePopover()">\n\n      L - Out of stock\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button class="out-of-stock" ion-item no-lines (click)="closePopover()">\n\n      XL - Out of stock\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\product-size-pop-over\product-size-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ProductSizePopOverPage);
    return ProductSizePopOverPage;
}());

//# sourceMappingURL=product-size-pop-over.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccountPage = /** @class */ (function () {
    function AccountPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    AccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-account',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\account\account.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>Account</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <img src="https://res.cloudinary.com/cediim8/image/upload/v1523427281/cover-photo.jpg">\n\n\n\n  <div text-center class="circle">\n\n    <p class="circle-text">JD</p>\n\n  </div>\n\n\n\n  <ion-list class="orders-list">\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-basket-outline"></ion-icon>\n\n      <p>My orders</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-archive-outline"></ion-icon>\n\n      <p>My returns</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="md-bicycle"></ion-icon>\n\n      <p>Premier Delivery</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n\n      <p>My details</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-home-outline"></ion-icon>\n\n      <p>Address Book</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-card"></ion-icon>\n\n      <p>Payment methods</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-text-outline"></ion-icon>\n\n      <p>Contact Preferences</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n\n      <p>Social accounts</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n\n      <p>Gift card & vouchers</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n\n      <p>Need help?</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n      <p>Where\'s my order?</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n\n      <p>How do I make a return?</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-item>\n\n    <ion-icon item-start name="md-log-out"></ion-icon>\n\n    <p>Sign out</p>\n\n  </ion-item>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\account\account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AccountPage);
    return AccountPage;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppSettingsPage = /** @class */ (function () {
    function AppSettingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AppSettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppSettingsPage');
    };
    AppSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-settings',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\app-settings\app-settings.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>APP SETTINGS</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-list-header>\n\n      <p>Store setup</p>\n\n    </ion-list-header>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Country</p>\n\n      <p>Australia</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Change currency</p>\n\n      <p>$ AUD</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Change language</p>\n\n      <p>Australian English</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Change sizes</p>\n\n      <p>Australian English</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-list-header>\n\n      <p>Other</p>\n\n    </ion-list-header>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Clear search history</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Help & Contact</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Report an issue</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Rate the app</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Notifications</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Clear search history</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Vibrate when you add to bag</p>\n\n      <ion-toggle item-end checked="false"></ion-toggle>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Terms and conditions</p>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list>\n\n    <ion-list-header>\n\n      <p>Other</p>\n\n    </ion-list-header>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Build version</p>\n\n      <p>1.0.0 (100)</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Build time</p>\n\n      <p>2018-01-01 12:00:00</p>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <p class="item-title">Open-source licenses</p>\n\n      <p>License details for used open-source software</p>\n\n    </ion-item>\n\n  </ion-list>\n\n'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\app-settings\app-settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AppSettingsPage);
    return AppSettingsPage;
}());

//# sourceMappingURL=app-settings.js.map

/***/ }),

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BagPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BagPage = /** @class */ (function () {
    function BagPage(navCtrl, navParams, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
    }
    BagPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BagPage');
    };
    BagPage.prototype.goToSavedItems = function () {
        this.navCtrl.push('SavedItemsPage');
    };
    BagPage.prototype.goToHome = function () {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    BagPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-bag',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\bag\bag.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>MY BAG</ion-title>\n\n\n\n    <ion-buttons right>\n\n      <button class="navbar-button" ion-button icon-only (click)="goToSavedItems()">\n\n        <ion-icon name="md-heart-outline"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-col text-center>\n\n    <img src="https://res.cloudinary.com/cediim8/image/upload/v1523427246/sad-face.png">\n\n\n\n    <p class="bag-empty">YOUR BAG IS EMPTY</p>\n\n\n\n    <p class="bag-description">Items are reserved for you in your bag for 60 minutes and are then moved to Saved Items.</p>\n\n\n\n    <button ion-button full (click)="goToSavedItems()">\n\n      VIEW SAVED ITEMS\n\n    </button>\n\n\n\n    <button class="start-shopping" ion-button full (click)="goToHome()">\n\n      START SHOPPING\n\n    </button>\n\n  </ion-col>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\bag\bag.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"]])
    ], BagPage);
    return BagPage;
}());

//# sourceMappingURL=bag.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenWomenCategoryPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenWomenCategoryPopOverPage = /** @class */ (function () {
    function MenWomenCategoryPopOverPage(navCtrl, navParams, viewController, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
        this.events = events;
    }
    MenWomenCategoryPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenWomenCategoryPopOverPage');
    };
    MenWomenCategoryPopOverPage.prototype.selectAndClosePopover = function (selection) {
        this.events.publish('genderShoppingChanged', selection == 'Men');
        this.viewController.dismiss();
    };
    MenWomenCategoryPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-men-women-category-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\men-women-category-pop-over\men-women-category-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover(\'Men\')">\n\n      Men\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover(\'Women\')">\n\n      Women\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\men-women-category-pop-over\men-women-category-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], MenWomenCategoryPopOverPage);
    return MenWomenCategoryPopOverPage;
}());

//# sourceMappingURL=men-women-category-pop-over.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpFaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HelpFaqPage = /** @class */ (function () {
    function HelpFaqPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HelpFaqPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpFaqPage');
    };
    HelpFaqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-help-faq',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\help-faq\help-faq.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>Help & FAQs</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\help-faq\help-faq.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], HelpFaqPage);
    return HelpFaqPage;
}());

//# sourceMappingURL=help-faq.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductColourPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductColourPopOverPage = /** @class */ (function () {
    function ProductColourPopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    ProductColourPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductColourPopOverPage');
    };
    ProductColourPopOverPage.prototype.closePopover = function () {
        this.viewController.dismiss();
    };
    ProductColourPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-product-colour-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\product-colour-pop-over\product-colour-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      BLACK\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      BLUE\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      RED\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      GREEN\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="closePopover()">\n\n      YELLOW\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\product-colour-pop-over\product-colour-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ProductColourPopOverPage);
    return ProductColourPopOverPage;
}());

//# sourceMappingURL=product-colour-pop-over.js.map

/***/ }),

/***/ 133:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 133;

/***/ }),

/***/ 175:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/account/account.module": [
		341,
		16
	],
	"../pages/app-settings/app-settings.module": [
		342,
		15
	],
	"../pages/bag/bag.module": [
		343,
		14
	],
	"../pages/category/category.module": [
		357,
		7
	],
	"../pages/filter-multi-selection/filter-multi-selection.module": [
		344,
		6
	],
	"../pages/filter-range/filter-range.module": [
		345,
		5
	],
	"../pages/filter/filter.module": [
		346,
		13
	],
	"../pages/help-faq/help-faq.module": [
		347,
		12
	],
	"../pages/men-women-category-pop-over/men-women-category-pop-over.module": [
		348,
		11
	],
	"../pages/product-colour-pop-over/product-colour-pop-over.module": [
		349,
		10
	],
	"../pages/product-details/product-details.module": [
		350,
		3
	],
	"../pages/product-details2/product-details2.module": [
		352,
		4
	],
	"../pages/product-size-pop-over/product-size-pop-over.module": [
		351,
		9
	],
	"../pages/products/products.module": [
		353,
		2
	],
	"../pages/programme/programme.module": [
		176
	],
	"../pages/qrcode/qrcode.module": [
		354,
		1
	],
	"../pages/recommended-pop-over/recommended-pop-over.module": [
		355,
		8
	],
	"../pages/saved-items/saved-items.module": [
		183
	],
	"../pages/sponsordetails/sponsordetails.module": [
		356,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 175;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgrammePageModule", function() { return ProgrammePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__programme__ = __webpack_require__(177);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProgrammePageModule = /** @class */ (function () {
    function ProgrammePageModule() {
    }
    ProgrammePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__programme__["a" /* ProgrammePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__programme__["a" /* ProgrammePage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__programme__["a" /* ProgrammePage */]
            ]
        })
    ], ProgrammePageModule);
    return ProgrammePageModule;
}());

//# sourceMappingURL=programme.module.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgrammePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_Providers_apiservice__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(83);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { BarcodeScanner } from "@ionic-native/barcode-scanner/index";
var ProgrammePage = /** @class */ (function () {
    function ProgrammePage(navCtrl, navParams, events, httpClient, https, ApiService, popoverCtrl, barcodeScanner) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.httpClient = httpClient;
        this.https = https;
        this.ApiService = ApiService;
        this.popoverCtrl = popoverCtrl;
        this.barcodeScanner = barcodeScanner;
        this.qrData = null;
        this.createdCode = null;
        this.scannedCode = null;
        this.homeSegment = "Details";
        this.event = {};
        this.EventID = navParams.get('data');
        this.loadEvent();
        this.loadSpecificEvent();
        this.loadEventMember();
        this.loadEventNonmember();
        this.loadEventGuest();
    }
    ProgrammePage.prototype.goToHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    ProgrammePage.prototype.scanCode = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            _this.scannedCode = barcodeData.text;
        });
    };
    ProgrammePage.prototype.loadEvent = function () {
        var _this = this;
        this.ApiService.getEvent()
            .subscribe(function (res) {
            var getEvent = res.json();
            _this.showEvent = getEvent;
        });
    };
    ProgrammePage.prototype.loadSpecificEvent = function () {
        var _this = this;
        this.ApiService.getSpecificEvent(this.EventID)
            .subscribe(function (res) {
            var getSpecificEvent = res.json();
            _this.showSpecEvent = getSpecificEvent.eventid;
        });
    };
    ProgrammePage.prototype.loadEventMember = function () {
        var _this = this;
        this.ApiService.getEventMember()
            .subscribe(function (res) {
            var getEventMember = res.json();
            _this.showEventMember = getEventMember;
        });
    };
    ProgrammePage.prototype.loadEventNonmember = function () {
        var _this = this;
        this.ApiService.getEventNonmember()
            .subscribe(function (res) {
            var getEventNonmember = res.json();
            _this.showEventNonmember = getEventNonmember;
        });
    };
    ProgrammePage.prototype.loadEventGuest = function () {
        var _this = this;
        this.ApiService.getEventGuest()
            .subscribe(function (res) {
            var getEventGuest = res.json();
            _this.showEventGuest = getEventGuest;
        });
    };
    ProgrammePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-programme',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\programme\programme.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>\n\n                Programme Page\n\n            </ion-title>\n\n    \n\n        </ion-navbar>\n\n    </ion-header>\n\n    \n\n    <ion-content padding>\n\n       \n\n      \n\n        <div>\n\n            <ion-segment [(ngModel)]="homeSegment" checked>\n\n        \n\n              <ion-segment-button value="Details" >\n\n                  <p class="item-title">Details</p>\n\n              </ion-segment-button>\n\n        \n\n              <ion-segment-button value="Attendance" >\n\n                <p class="item-title">Attendance</p>\n\n            </ion-segment-button>  \n\n            \n\n            <ion-segment-button value="qrscan" >\n\n              <p class="item-title">Scan QR</p>\n\n          </ion-segment-button>  \n\n        \n\n            </ion-segment>\n\n        \n\n        \n\n          </div>\n\n\n\n          <div [ngSwitch]="homeSegment">\n\n\n\n  <div *ngSwitchCase="\'Details\'">\n\n\n\n            <ion-grid *ngFor="let getspecevent of showSpecEvent">\n\n                \n\n\n\n         \n\n                <img style="border-radius: 9px" src="http://admin.acccim-registration.org.my{{getSpecEvent.EventImage}}" >\n\n              \n\n            \n\n\n\n          \n\n                <p  style="margin-top: 8px;"><b>{{getSpecEvent.EventTitle}}</b></p>           \n\n                <h3 style="margin-top: 8px;"><b>Date :</b>{{getSpecEvent.StartDate}}</h3>\n\n                <h3 style="margin-top: 8px;"><b>Time :</b>{{getSpecEvent.StartTime}}</h3>\n\n                <!--<div *ngIf="getevent && getevent.MemberAvailability_tb && getevent.MemberAvailability_tb[0]"></div>-->\n\n               \n\n           \n\n             \n\n        </ion-grid>             \n\n       </div>   \n\n       \n\n       \n\n       <div *ngSwitchCase="\'Attendance\'">\n\n\n\n            <ion-list class="menu-list" no-lines>\n\n            \n\n                    <ion-item class="side-menu-item" >\n\n                       \n\n                        <ion-card>\n\n                            <ion-grid>\n\n                                <ion-row >\n\n                                  <ion-col col-2>\n\n                \n\n                            <ion-avatar style="margin-top: 10px;">\n\n                                <img style="width: 50px; height: 50px;" src="../../assets/images/Profile_wangyuhen2.jpg" >\n\n                              </ion-avatar>\n\n                            \n\n                \n\n                            </ion-col>\n\n                \n\n                            <ion-col col-5> \n\n                                          \n\n                                <h3 style="margin-top: 8px;">Wang Yu Hen</h3>\n\n                                <p>Member</p> \n\n                                <p>YEC Forum 2</p>   \n\n                              </ion-col>   \n\n          \n\n                              <ion-col col-1>   \n\n                                  <ion-icon style="margin-top: 23px; color:green " ios="ios-checkmark-circle" md="md-checkmark-circle"></ion-icon>                      </ion-col>    \n\n                                \n\n                                <ion-col col-4> \n\n                                          \n\n                                   <p style="margin-top: 13px;">Date : 29/7/2019</p>\n\n                                   <p>Time : 7.03pm</p>\n\n                                  </ion-col>   \n\n          \n\n                          </ion-row>\n\n                        </ion-grid>             \n\n                              </ion-card>\n\n            \n\n            \n\n                              <ion-card>\n\n                                  <ion-grid>\n\n                                      <ion-row >\n\n                                        <ion-col col-2>\n\n                      \n\n                                  <ion-avatar style="margin-top: 10px;">\n\n                                      <img style="width: 50px; height: 50px;" src="../../assets/images/Profile_dominic2.jpg" >\n\n                                    </ion-avatar>\n\n                                  \n\n                      \n\n                                  </ion-col>\n\n                      \n\n                                  <ion-col col-5> \n\n                                                \n\n                                      <h3 style="margin-top: 8px;">Dominic Teh</h3>\n\n                                      <p>Invited Guest Local</p> \n\n                                      <p>YEC Forum 2</p>   \n\n                                    </ion-col>   \n\n                \n\n                                    <ion-col col-1>   \n\n                                        <ion-icon style="margin-top: 23px; color:red; " ios="ios-close-circle" md="md-close-circle"></ion-icon>                      </ion-col>    \n\n                                      \n\n                                      <ion-col col-4> \n\n                                                \n\n                                         <p style="margin-top: 13px;">Date : 29/7/2019</p>\n\n                                         <p>Time : 7.03pm</p>\n\n                                        </ion-col>   \n\n                \n\n                                </ion-row>\n\n                              </ion-grid>             \n\n                                    </ion-card>\n\n            \n\n                                    <ion-card>\n\n                                        <ion-grid>\n\n                                            <ion-row >\n\n                                              <ion-col col-2>\n\n                            \n\n                                        <ion-avatar style="margin-top: 10px;">\n\n                                            <img style="width: 50px; height: 50px;" src="../../assets/images/profile_julian.JPG" >\n\n                                          </ion-avatar>\n\n                                        \n\n                            \n\n                                        </ion-col>\n\n                            \n\n                                        <ion-col col-5> \n\n                                                      \n\n                                            <h3 style="margin-top: 8px;">Julian Saik Weng Kong</h3>\n\n                                            <p>Commitee</p> \n\n                                            <p>YEC Forum 2</p>   \n\n                                          </ion-col>   \n\n                      \n\n                                          <ion-col col-1>   \n\n                                              <ion-icon style="margin-top: 23px; color:#ED6D10; " ios="ios-remove-circle" md="md-remove-circle"></ion-icon>                      </ion-col>    \n\n                                            \n\n                                            <ion-col col-4> \n\n                                                      \n\n                                               <p style="margin-top: 13px;">Date : 29/7/2019</p>\n\n                                               <p>Time : 7.03pm</p>\n\n                                              </ion-col>   \n\n                      \n\n                                      </ion-row>\n\n                                    </ion-grid>    \n\n                                    </ion-card>         \n\n                                          \n\n                                               \n\n                    </ion-item>\n\n            \n\n                  </ion-list>\n\n\n\n        </div>\n\n\n\n\n\n        <div *ngSwitchCase="\'qrscan\'">\n\n<br>\n\n          <button ion-button full icon-left (click)="scanCode()"><ion-icon name="qr-scanner"></ion-icon>Scan Code</button>\n\n\n\n      \n\n          <ion-card *ngIf="scannedCode">\n\n                  <ion-card-content>\n\n                      <p>Result from Scan: {{ scannedCode }}</p>\n\n                  </ion-card-content>\n\n          \n\n              </ion-card>\n\n      \n\n\n\n        </div>\n\n                  \n\n    </div>\n\n    </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\programme\programme.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__app_Providers_apiservice__["a" /* ApiserviceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__app_Providers_apiservice__["a" /* ApiserviceProvider */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__["a" /* BarcodeScanner */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]) === "function" && _h || Object])
    ], ProgrammePage);
    return ProgrammePage;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());

//# sourceMappingURL=programme.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavedItemsPageModule", function() { return SavedItemsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saved_items__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavedItemsPageModule = /** @class */ (function () {
    function SavedItemsPageModule() {
    }
    SavedItemsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__saved_items__["a" /* SavedItemsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__saved_items__["a" /* SavedItemsPage */]),
            ],
        })
    ], SavedItemsPageModule);
    return SavedItemsPageModule;
}());

//# sourceMappingURL=saved-items.module.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popupmodal_popupmodal__ = __webpack_require__(226);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterPage = /** @class */ (function () {
    function RegisterPage(nav) {
        this.nav = nav;
    }
    // register and go to home page
    RegisterPage.prototype.register = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__popupmodal_popupmodal__["a" /* PopupModalPage */]);
    };
    // go to login page
    RegisterPage.prototype.login = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\register\register.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!-- -->\n\n<ion-content class="auth-page">\n\n  <div class="login-content">\n\n\n\n    <!-- Logo -->\n\n    <div padding text-center>\n\n      <div class="logo"></div>\n\n      <h2 ion-text class="text-primary">\n\n        <strong>Ionic 3</strong> Start Theme\n\n      </h2>\n\n    </div>\n\n\n\n    <!-- Login form -->\n\n    <form class="list-form">\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="person" item-start class="text-primary"></ion-icon>\n\n          Full Name\n\n        </ion-label>\n\n        <ion-input type="text"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="mail" item-start class="text-primary"></ion-icon>\n\n          Email\n\n        </ion-label>\n\n        <ion-input type="email"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n          Password\n\n        </ion-label>\n\n        <ion-input type="password"></ion-input>\n\n      </ion-item>\n\n    </form>\n\n\n\n    <div margin-top>\n\n      <button ion-button block color="dark" tappable (click)="register()">\n\n        SIGN UP\n\n      </button>\n\n\n\n      <p text-center ion-text color="secondary">Or Sign Up with:</p>\n\n\n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col col-4>\n\n            <button ion-button icon-only block class="btn-facebook">\n\n              <ion-icon name="logo-facebook"></ion-icon>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col col-4>\n\n            <button ion-button icon-only block class="btn-twitter">\n\n              <ion-icon name="logo-twitter"></ion-icon>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col col-4>\n\n            <button ion-button icon-only block class="btn-gplus">\n\n              <ion-icon name="logo-googleplus"></ion-icon>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </div>\n\n\n\n    <!-- Other links -->\n\n    <div text-center margin-top>\n\n      <span ion-text color="primary" tappable (click)="login()">I have an account</span>\n\n    </div>\n\n\n\n  </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\register\register.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopupModalPage = /** @class */ (function () {
    function PopupModalPage(nav) {
        this.nav = nav;
    }
    PopupModalPage.prototype.goToHome = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    PopupModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-popupmodal',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\popupmodal\popupmodal.html"*/'<ion-header>\n\n       \n\n      </ion-header>\n\n      \n\n      <ion-content padding class="bg-style">\n\n          <br> \n\n\n\n          <ion-card>\n\n\n\n                <img src="../../assets/images/Profile_dominic2.jpg">\n\n                <ion-fab right top>\n\n                  <button ion-fab (click)="goToHome()">\n\n                        <ion-icon ios="ios-close" md="md-close" ></ion-icon>\n\n                  </button>\n\n                </ion-fab>\n\n              \n\n              \n\n                <ion-item>\n\n                \n\n                  <button ion-button icon-start clear item-end (click)="goToHome()">\n\n                        <ion-icon ios="ios-close-circle" md="md-close-circle" ></ion-icon>\n\n                       \n\n                    Close\n\n                  </button>\n\n          \n\n                </ion-item>\n\n              \n\n              </ion-card>\n\n              \n\n\n\n                    \n\n      </ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\popupmodal\popupmodal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], PopupModalPage);
    return PopupModalPage;
}());

//# sourceMappingURL=popupmodal.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FilterPage = /** @class */ (function () {
    function FilterPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    FilterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilterPage');
    };
    FilterPage.prototype.closeFilter = function () {
        this.viewController.dismiss();
    };
    FilterPage.prototype.goToMultiSelection = function (selectionType) {
        this.navCtrl.push('FilterMultiSelectionPage', { selectionType: selectionType });
    };
    FilterPage.prototype.goToRangeSelection = function (selectionType) {
        this.navCtrl.push('FilterRangePage', { selectionType: selectionType });
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\filter\filter.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>FILTER</ion-title>\n\n\n\n    <ion-buttons right>\n\n      <button class="navbar-button" ion-button icon-only (click)="closeFilter()">\n\n        <ion-icon name="md-close-circle"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-item (click)="goToMultiSelection(\'Product Type\')">\n\n    <p class="filter-type" item-start>Product Type</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Range\')">\n\n    <p class="filter-type" item-start>Range</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Brand\')">\n\n    <p class="filter-type" item-start>Brand</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Multipacks\')">\n\n    <p class="filter-type" item-start>Multipacks</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Size\')">\n\n    <p class="filter-type" item-start>Size</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToRangeSelection(\'Price Range\')">\n\n    <p class="filter-type" item-start>Price Range</p>\n\n    <p item-end>$20 - $500</p>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="goToMultiSelection(\'Colour\')">\n\n    <p class="filter-type" item-start>Colour</p>\n\n    <p item-end>All</p>\n\n  </ion-item>\n\n</ion-content>\n\n\n\n<ion-footer padding>\n\n  <button ion-button full (click)="closeFilter()">\n\n    <p>DONE</p>\n\n  </button>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\filter\filter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecommendedPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecommendedPopOverPage = /** @class */ (function () {
    function RecommendedPopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    RecommendedPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RecommendedPopOverPage');
    };
    RecommendedPopOverPage.prototype.selectAndClosePopover = function () {
        this.viewController.dismiss();
    };
    RecommendedPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-recommended-pop-over',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\recommended-pop-over\recommended-pop-over.html"*/'<ion-list no-margin no-lines>\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      Recommended\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      What\'s New\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      Price - Low to High\n\n    </button>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n\n      Price - High to Low\n\n    </button>\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\recommended-pop-over\recommended-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], RecommendedPopOverPage);
    return RecommendedPopOverPage;
}());

//# sourceMappingURL=recommended-pop-over.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(254);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 254:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bag_bag__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_saved_items_saved_items__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_account_account__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_saved_items_saved_items_module__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_popupmodal_popupmodal__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_login_login__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_register_register__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_programme_programme__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_ionic4_alpha_scroll__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_ionic4_alpha_scroll___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_22_ionic4_alpha_scroll__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ngx_qrcode2__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_barcode_scanner__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_common_http__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_http__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__app_Providers_apiservice__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_programme_programme_module__ = __webpack_require__(176);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__["a" /* RecommendedPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_bag_bag__["a" /* BagPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__["a" /* AppSettingsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__["a" /* HelpFaqPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_popupmodal_popupmodal__["a" /* PopupModalPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_register_register__["a" /* RegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/account/account.module#AccountPageModule', name: 'AccountPage', segment: 'account', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/app-settings/app-settings.module#AppSettingsPageModule', name: 'AppSettingsPage', segment: 'app-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bag/bag.module#BagPageModule', name: 'BagPage', segment: 'bag', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter-multi-selection/filter-multi-selection.module#FilterMultiSelectionPageModule', name: 'FilterMultiSelectionPage', segment: 'filter-multi-selection', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter-range/filter-range.module#FilterRangePageModule', name: 'FilterRangePage', segment: 'filter-range', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter/filter.module#FilterPageModule', name: 'FilterPage', segment: 'filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/help-faq/help-faq.module#HelpFaqPageModule', name: 'HelpFaqPage', segment: 'help-faq', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/men-women-category-pop-over/men-women-category-pop-over.module#MenWomenCategoryPopOverPageModule', name: 'MenWomenCategoryPopOverPage', segment: 'men-women-category-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-colour-pop-over/product-colour-pop-over.module#ProductColourPopOverPageModule', name: 'ProductColourPopOverPage', segment: 'product-colour-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-details/product-details.module#ProductDetailsPageModule', name: 'ProductDetailsPage', segment: 'product-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-size-pop-over/product-size-pop-over.module#ProductSizePopOverPageModule', name: 'ProductSizePopOverPage', segment: 'product-size-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-details2/product-details2.module#ProductDetailsPageModule', name: 'ProductDetails2Page', segment: 'product-details2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/products/products.module#ProductsPageModule', name: 'ProductsPage', segment: 'products', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/programme/programme.module#ProgrammePageModule', name: 'ProgrammePage', segment: 'programme', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/qrcode/qrcode.module#QRcodePageModule', name: 'QRcodePage', segment: 'qrcode', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/recommended-pop-over/recommended-pop-over.module#RecommendedPopOverPageModule', name: 'RecommendedPopOverPage', segment: 'recommended-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/saved-items/saved-items.module#SavedItemsPageModule', name: 'SavedItemsPage', segment: 'saved-items', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sponsordetails/sponsordetails.module#SponsorDetailsPageModule', name: 'SponsorDetailsPage', segment: 'sponsordetails', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/category/category.module#CategoryPageModule', name: 'CategoryPage', segment: 'category', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_13__pages_saved_items_saved_items_module__["SavedItemsPageModule"],
                __WEBPACK_IMPORTED_MODULE_25__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_22_ionic4_alpha_scroll__["IonAlphaScrollModule"],
                __WEBPACK_IMPORTED_MODULE_28__pages_programme_programme_module__["ProgrammePageModule"],
                __WEBPACK_IMPORTED_MODULE_23_ngx_qrcode2__["a" /* NgxQRCodeModule */],
                __WEBPACK_IMPORTED_MODULE_26__angular_http__["b" /* HttpModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__["a" /* RecommendedPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_bag_bag__["a" /* BagPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_saved_items_saved_items__["a" /* SavedItemsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__["a" /* AppSettingsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__["a" /* HelpFaqPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_popupmodal_popupmodal__["a" /* PopupModalPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_programme_programme__["a" /* ProgrammePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_register_register__["a" /* RegisterPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_27__app_Providers_apiservice__["a" /* ApiserviceProvider */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_bag_bag__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_saved_items_saved_items__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_account_account__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_app_settings_app_settings__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_help_faq_help_faq__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.goToHome = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
    };
    MyApp.prototype.goToBag = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_bag_bag__["a" /* BagPage */]);
    };
    MyApp.prototype.goToSavedItems = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_saved_items_saved_items__["a" /* SavedItemsPage */]);
    };
    MyApp.prototype.goToMyAccount = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_account_account__["a" /* AccountPage */]);
    };
    MyApp.prototype.goToAppSettings = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_app_settings_app_settings__["a" /* AppSettingsPage */]);
    };
    MyApp.prototype.goToHelpAndFaq = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_help_faq_help_faq__["a" /* HelpFaqPage */]);
    };
    MyApp.prototype.goToLogin = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title class="welcome-title">\n\n        <p>Hey,\n\n          <strong>Julian Saik</strong>\n\n        </p>\n\n      </ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list class="menu-list" no-lines>\n\n      \n\n      <ion-item class="side-menu-item" (click)="goToHome()">\n\n        <ion-icon name="ios-home-outline" item-start></ion-icon>\n\n        <button menuClose ion-item>HOME</button>\n\n      </ion-item>\n\n    \n\n      <!--\n\n      <ion-item class="side-menu-item" (click)="goToBag()">\n\n        <ion-icon name="ios-basket-outline" item-start></ion-icon>\n\n        <button menuClose ion-item>BAG</button>\n\n      </ion-item>\n\n\n\n      <ion-item class="side-menu-item" (click)="goToSavedItems()">\n\n        <ion-icon name="md-heart-outline" item-start></ion-icon>\n\n        <button menuClose ion-item>SAVED ITEMS</button>\n\n      </ion-item>\n\n    -->\n\n      <ion-item class="side-menu-item" (click)="goToMyAccount()">\n\n        <ion-icon name="ios-person-outline" item-start></ion-icon>\n\n        <button menuClose ion-item>MY ACCOUNT</button>\n\n      </ion-item>\n\n\n\n     \n\n      <ion-item class="side-menu-item" (click)="goToAppSettings()">\n\n        <ion-icon name="ios-settings-outline" item-start></ion-icon>\n\n        <button menuClose ion-item>APP SETTINGS</button>\n\n      </ion-item>\n\n    \n\n\n\n      <ion-item class="side-menu-item" (click)="goToHelpAndFaq()">\n\n        <ion-icon name="ios-information-circle-outline" item-start></ion-icon>\n\n        <button menuClose ion-item>HELP & FAQS</button>\n\n      </ion-item>\n\n\n\n      <ion-item class="side-menu-item" (click)="goToLogin()">\n\n          <ion-icon name="md-log-out" item-start></ion-icon>\n\n          <button menuClose ion-item>SIGN OUT</button>\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n\n\n   \n\n<!--\n\n    <ion-list class="menu-list">\n\n      <ion-list-header>\n\n        <p>MORE ASOS</p>\n\n      </ion-list-header>\n\n\n\n      <ion-item>\n\n        <p>Unlimited Express Shipping</p>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <p>10% Student Discount</p>\n\n      </ion-item>\n\n    </ion-list>\n\n  -->\n\n    <ion-list class="menu-list">\n\n      <ion-list-header>\n\n        <p>TELL US WHAT YOU THINK</p>\n\n      </ion-list-header>\n\n\n\n \n\n\n\n      <ion-item>\n\n        <p>Rate the App</p>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    \n\n    <ion-item>\n\n      <p>App Version 1.0.0</p>\n\n    </ion-item>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__men_women_category_pop_over_men_women_category_pop_over__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_Providers_apiservice__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, popoverCtrl, modal, events, navParams, ApiService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.modal = modal;
        this.events = events;
        this.ApiService = ApiService;
        this.homeSegment = "home";
        this.isMenSelected = true;
        this.pageTitle = 'MEN';
        this.event = {};
        this.images = ['../../assets/images/Profile_wangyuhen2.jpg', '../../assets/images/Profile_dominic2.jpg', '../../assets/images/Profile_zainul1.jpg', '../../assets/images/Profile_ryan1.jpg'];
        this.d_images = ['../../assets/images/a_diamond.png'];
        this.p_images = ['../../assets/images/a_platinum.png', '../../assets/images/a_platinum.png', '../../assets/images/a_platinum.png'];
        this.g_images = ['../../assets/images/a_gold.png', '../../assets/images/a_gold.png', '../../assets/images/a_gold.png', '../../assets/images/a_gold.png', '../../assets/images/a_gold.png'];
        this.s1_images = ['../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png'];
        this.s2_images = ['../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png'];
        this.s3_images = ['../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png', '../../assets/images/a_silverbronze.png'];
        this.visitorview = "member";
        events.subscribe('genderShoppingChanged', function (isMenSelected) {
            _this.isMenSelected = isMenSelected;
            _this.pageTitle = _this.isMenSelected ? 'MEN' : 'WOMEN';
        });
        this.user = navParams.get('data');
        this.pass = navParams.get('data1');
        this.loadAdminDetails();
        this.loadEvent();
        this.loadEventMember();
        this.loadEventNonmember();
        this.loadEventGuest();
    }
    HomePage.prototype.loadAdminDetails = function () {
        var _this = this;
        this.ApiService.adminLogin(this.user, this.pass)
            .subscribe(function (res) {
            var getUser = res.json();
            _this.showuser = getUser;
        });
    };
    HomePage.prototype.loadEvent = function () {
        var _this = this;
        this.ApiService.getEvent()
            .subscribe(function (res) {
            var getEvent = res.json();
            _this.showEvent = getEvent;
            //console.log(this.showEvent.MemberAvailability_tb.MemberType);
        });
    };
    HomePage.prototype.loadEventMember = function () {
        var _this = this;
        this.ApiService.getEventMember()
            .subscribe(function (res) {
            var getEventMember = res.json();
            _this.showEventMember = getEventMember;
        });
    };
    HomePage.prototype.loadEventNonmember = function () {
        var _this = this;
        this.ApiService.getEventNonmember()
            .subscribe(function (res) {
            var getEventNonmember = res.json();
            _this.showEventNonmember = getEventNonmember;
        });
    };
    HomePage.prototype.loadEventGuest = function () {
        var _this = this;
        this.ApiService.getEventGuest()
            .subscribe(function (res) {
            var getEventGuest = res.json();
            _this.showEventGuest = getEventGuest;
        });
    };
    //Move to Next slide
    HomePage.prototype.slideNext = function (object, slideView) {
        var _this = this;
        slideView.slideNext(500).then(function () {
            _this.checkIfNavDisabled(object, slideView);
        });
    };
    //Move to previous slide
    HomePage.prototype.slidePrev = function (object, slideView) {
        var _this = this;
        slideView.slidePrev(500).then(function () {
            _this.checkIfNavDisabled(object, slideView);
        });
        ;
    };
    //Method called when slide is changed by drag or navigation
    HomePage.prototype.SlideDidChange = function (object, slideView) {
        this.checkIfNavDisabled(object, slideView);
    };
    //Call methods to check if slide is first or last to enable disbale navigation  
    HomePage.prototype.checkIfNavDisabled = function (object, slideView) {
        this.checkisBeginning(object, slideView);
        this.checkisEnd(object, slideView);
    };
    HomePage.prototype.checkisBeginning = function (object, slideView) {
        slideView.isBeginning().then(function (istrue) {
            object.isBeginningSlide = istrue;
        });
    };
    HomePage.prototype.checkisEnd = function (object, slideView) {
        slideView.isEnd().then(function (istrue) {
            object.isEndSlide = istrue;
        });
    };
    HomePage.prototype.showPopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    HomePage.prototype.goToCategory = function (category) {
        var navParams = {
            category: category,
            isMenSelected: this.isMenSelected
        };
        this.navCtrl.push('CategoryPage', navParams);
    };
    HomePage.prototype.goToProducts = function () {
        var navParams = {
            category: 'CLOTHING',
            isMenSelected: this.isMenSelected
        };
        this.navCtrl.push('ProductDetailsPage', navParams);
    };
    HomePage.prototype.changeTab = function (value) {
        this.visitorview = value;
    };
    HomePage.prototype.goToSponsorDetails = function () {
        var navParams = {
            isMenSelected: this.isMenSelected
        };
        this.navCtrl.push('SponsorDetailsPage', navParams);
    };
    HomePage.prototype.goToQR = function () {
        this.navCtrl.push('QRcodePage');
    };
    HomePage.prototype.gotoProgramme = function () {
        this.navCtrl.push('ProgrammePage');
    };
    HomePage.prototype.goToSavedItems = function () {
        this.navCtrl.push('SavedItemsPage');
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n  \n\n\n\n    <ion-title>\n\n<h2> Admin</h2>\n\n      <!---\n\n        <button class="navbar-button" ion-button clear (click)="showPopOver($event)">\n\n        <p>{{ pageTitle }}</p>\n\n        <ion-icon name="ios-arrow-down"></ion-icon>\n\n      </button>\n\n    -->\n\n    </ion-title>\n\n\n\n    <ion-buttons right>\n\n\n\n      <!--\n\n      <button class="navbar-button" ion-button icon-only (click)="goToSavedItems()">\n\n        <ion-icon name="md-heart-outline"></ion-icon>\n\n      </button>\n\n    -->\n\n\n\n\n\n      <button class="navbar-button" ion-button icon-only>\n\n\n\n        <ion-icon ios="ios-search" md="md-search"></ion-icon>\n\n        <ion-icon ios="ios-settings" md="md-settings"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div>\n\n    <ion-segment [(ngModel)]="homeSegment" checked>\n\n\n\n      <ion-segment-button value="home" >\n\n          <p class="item-title">Programme</p>\n\n      </ion-segment-button>\n\n\n\n      <ion-segment-button click="changeTab(\'member\')" value="visitor" >\n\n        <p class="item-title">Visitor</p>\n\n    </ion-segment-button>\n\n\n\n\n\n    \n\n    </ion-segment>\n\n\n\n\n\n  </div>\n\n\n\n  <div [ngSwitch]="homeSegment">\n\n\n\n\n\n      <div *ngSwitchCase="\'home\'">\n\n  \n\n          <br>\n\n          \n\n          \n\n          <ion-list class="menu-list" no-lines>\n\n                      \n\n            \n\n              <ion-item class="side-menu-item" >\n\n          \n\n          \n\n                  <ion-grid>\n\n                    <!--\n\n                      <ion-row >\n\n                        <ion-col col-1>\n\n                  <ion-icon ios="ios-options" md="md-options" style="color:#ED6D10;"></ion-icon>\n\n                        </ion-col>\n\n          \n\n                        <ion-col col-3>\n\n                            <p>Filter</p>\n\n                                  </ion-col>\n\n          \n\n          \n\n                        </ion-row>\n\n                      -->\n\n                        </ion-grid>\n\n                 \n\n                  <ion-card *ngFor="let getevent of showEvent">\n\n                      <ion-grid  (click)="gotoProgramme()">\n\n                          <ion-row >\n\n                            <ion-col col-6>\n\n          \n\n                   \n\n                          <img style="width: 170px; height: 120px; border-radius: 9px" src="http://admin.acccim-registration.org.my{{getevent.EventImage}}" >\n\n                        \n\n                      \n\n          \n\n                      </ion-col>\n\n          \n\n                      <ion-col col-6> \n\n                          <ion-item text-wrap>\n\n                          <p style="margin-top: 8px;"><b>{{getevent.EventTitle}}</b></p>           \n\n                          <h3 style="margin-top: 8px;"><b>Date :</b>{{getevent.StartDate}}</h3>\n\n                          <h3 style="margin-top: 8px;"><b>Time :</b>{{getevent.StartTime}}</h3>\n\n                          <!--<div *ngIf="getevent && getevent.MemberAvailability_tb && getevent.MemberAvailability_tb[0]"></div>-->\n\n                          <h3 style="margin-top: 8px;"><b>Opened For :</b></h3>\n\n                            <div *ngFor="let gett of getevent.MemberAvailability_tb">\n\n                              <h3>{{gett.MemberType}}</h3>\n\n                            <!--</div>-->\n\n                            \n\n                          </div>\n\n                        </ion-item>\n\n                          \n\n                     \n\n                        </ion-col>                    \n\n                    </ion-row>\n\n                  </ion-grid>             \n\n                        </ion-card>\n\n                                             \n\n              </ion-item>\n\n          \n\n            </ion-list>\n\n          \n\n                \n\n           \n\n            \n\n              </div>\n\n\n\n\n\n\n\n    <div *ngSwitchCase="\'visitor\'">\n\n\n\n <!--\n\n      <ion-row>\n\n        <button class="freeDeliveryReturns" ion-button full>\n\n          <p>Free delivery and returns</p>\n\n        </button>\n\n      </ion-row>\n\n    -->\n\n\n\n    <ion-segment [(ngModel)]="visitorview">\n\n\n\n      <ion-segment-button value="member">\n\n          <p class="item-title">Member</p>\n\n          \n\n      </ion-segment-button>\n\n\n\n      <ion-segment-button value="nonmember">\n\n          <p class="item-title">Non-member</p>\n\n      </ion-segment-button>\n\n\n\n      <ion-segment-button value="guest">\n\n          <p class="item-title">Guest</p>\n\n      </ion-segment-button>\n\n\n\n    </ion-segment>\n\n\n\n    <div [ngSwitch]="visitorview">\n\n      <div *ngSwitchCase="\'member\'">\n\n    <ion-list class="menu-list" no-lines>\n\n            \n\n        <ion-item class="side-menu-item">\n\n\n\n            <ion-card *ngFor="let getmember of showEventMember">\n\n                <ion-grid>\n\n                    <ion-row style="margin-top: 5px;">\n\n                      <!--<ion-col col-4>\n\n    \n\n                <ion-avatar>\n\n                    <img style="width: 50px; height: 50px;" src="http://admin.acccim-registration.org.my{{getuser.GridListing}}" >\n\n                  </ion-avatar>\n\n                \n\n    \n\n                </ion-col>-->\n\n    \n\n                <ion-col col-12> \n\n                              \n\n                    <h3 style="margin-top: 8px;">{{getmember.EnglishName}}</h3>\n\n                    <p>{{getmember.UserType}}</p>   \n\n                  </ion-col>                    \n\n              </ion-row>\n\n                </ion-grid>             \n\n            </ion-card>\n\n\n\n                                        \n\n        </ion-item>\n\n\n\n      </ion-list>\n\n     \n\n     </div>\n\n\n\n     <div *ngSwitchCase="\'nonmember\'">\n\n        <ion-list class="menu-list" no-lines>\n\n                \n\n            <ion-item class="side-menu-item">\n\n    \n\n                <ion-card *ngFor="let getnonmember of showEventNonmember ">\n\n                    <ion-grid>\n\n                        <ion-row style="margin-top: 5px;">\n\n                          <!--<ion-col col-4>\n\n        \n\n                    <ion-avatar>\n\n                        <img style="width: 50px; height: 50px;" src="http://admin.acccim-registration.org.my{{getuser.GridListing}}" >\n\n                      </ion-avatar>\n\n                    \n\n        \n\n                    </ion-col>-->\n\n        \n\n                    <ion-col col-12> \n\n                                  \n\n                        <h3 style="margin-top: 8px;">{{getnonmember.EnglishName}}</h3>\n\n                        <p>{{getnonmember.UserType}}</p>   \n\n                      </ion-col>                    \n\n                  </ion-row>\n\n                    </ion-grid>             \n\n                </ion-card>\n\n    \n\n                                            \n\n            </ion-item>\n\n    \n\n          </ion-list>\n\n         \n\n         </div>\n\n\n\n\n\n         <div *ngSwitchCase="\'guest\'">\n\n            <ion-list class="menu-list" no-lines>\n\n                    \n\n                <ion-item class="side-menu-item">\n\n        \n\n                    <ion-card *ngFor="let getguest of showEventGuest ">\n\n                        <ion-grid>\n\n                            <ion-row style="margin-top: 5px;">\n\n                              <!--<ion-col col-4>\n\n            \n\n                        <ion-avatar>\n\n                            <img style="width: 50px; height: 50px;" src="http://admin.acccim-registration.org.my{{getuser.GridListing}}" >\n\n                          </ion-avatar>\n\n                        \n\n            \n\n                        </ion-col>-->\n\n            \n\n                        <ion-col col-12> \n\n                                      \n\n                            <h3 style="margin-top: 8px;">{{getguest.EnglishName}}</h3>\n\n                            <p>{{getguest.UserType}}</p>   \n\n                          </ion-col>                    \n\n                      </ion-row>\n\n                        </ion-grid>             \n\n                    </ion-card>\n\n        \n\n                                                \n\n                </ion-item>\n\n        \n\n              </ion-list>\n\n             \n\n             </div>\n\n\n\n      \n\n    \n\n<!--\n\n      <ion-grid>\n\n          <ion-row>\n\n              <ion-col col-4>\n\n              </ion-col>\n\n\n\n            <ion-col col-4 center>\n\n                \n\n                <ion-grid>\n\n                    <ion-row>\n\n                        <ion-col col-2>\n\n                        </ion-col>\n\n                        <ion-col col-8>\n\n                            <ion-avatar>\n\n                                  <img style="width: 90px; height: 90px; " src="../../assets/images/qr-code-orange.png"\n\n                                  (click)="goToQR()"  >\n\n                              \n\n                                </ion-avatar>\n\n\n\n                              <p style="margin-top: 8px;"> &nbsp; scan qr code</p>\n\n                        </ion-col>\n\n\n\n                        <ion-col col-2></ion-col>\n\n              \n\n\n\n                  \n\n          </ion-row>\n\n      </ion-grid>\n\n\n\n      \n\n\n\n    </ion-col>\n\n\n\n\n\n    <ion-col col-4>\n\n      </ion-col>\n\n\n\n    </ion-row>\n\n    </ion-grid>\n\n  -->\n\n      \n\n    </div>\n\n    </div>\n\n\n\n\n\n</div>\n\n\n\n\n\n    <script src="home.js"></script>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__app_Providers_apiservice__["a" /* ApiserviceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__app_Providers_apiservice__["a" /* ApiserviceProvider */]) === "function" && _f || Object])
    ], HomePage);
    return HomePage;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiserviceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ListingServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
//Declare a httpOptions and mention the header in json format so that HTTP Post method can use this option.
var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var ApiserviceProvider = /** @class */ (function () {
    function ApiserviceProvider(http, https) {
        this.http = http;
        this.https = https;
        this.AdminLoginAPI = "https://api.acccim-registration.org.my/api/AdminSignIn?id=";
        this.eventListing = "https://api.acccim-registration.org.my/api/EventListing";
        this.specificevent = "https://api.acccim-registration.org.my/api/SpecificEvent?eventid=";
        this.eventguest = "https://api.acccim-registration.org.my/api/Guest";
        this.eventmember = "https://api.acccim-registration.org.my/api/Member";
        this.eventnonmember = "https://api.acccim-registration.org.my/api/NonMember";
    }
    //User Login
    ApiserviceProvider.prototype.adminLogin = function (user, pass) {
        var id = user;
        var pw = pass;
        if (id != null && pw != null) {
            return this.https.get(this.AdminLoginAPI + id + '&&pw=' + pw);
        }
    };
    ApiserviceProvider.prototype.DisplayAdmin = function (user, pass) {
        var id = user;
        var pw = pass;
        return this.https.get(this.AdminLoginAPI + id + '&&pw=' + pw);
    };
    ApiserviceProvider.prototype.getEventListing = function () {
        return this.https.get(this.eventListing);
    };
    ApiserviceProvider.prototype.getEvent = function () {
        return this.https.get(this.eventListing);
    };
    ApiserviceProvider.prototype.getSpecificEvent = function (EventID) {
        var eventid = EventID;
        return this.https.get(this.specificevent + eventid);
    };
    ApiserviceProvider.prototype.getEventGuest = function () {
        return this.https.get(this.eventguest);
    };
    ApiserviceProvider.prototype.getEventMember = function () {
        return this.https.get(this.eventmember);
    };
    ApiserviceProvider.prototype.getEventNonmember = function () {
        return this.https.get(this.eventnonmember);
    };
    ApiserviceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]) === "function" && _b || Object])
    ], ApiserviceProvider);
    return ApiserviceProvider;
    var _a, _b;
}());

//# sourceMappingURL=apiservice.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavedItemsPage; });
/* unused harmony export Item */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_colour_pop_over_product_colour_pop_over__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SavedItemsPage = /** @class */ (function () {
    function SavedItemsPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.savedItems = [];
        this.itemsCount = 0;
        this.getExampleItems();
    }
    SavedItemsPage.prototype.getExampleItems = function () {
        // Discounted item
        var discountedItem = new Item();
        discountedItem.name = "Pull&Bear Denim Jacket In Black";
        discountedItem.price = 40;
        discountedItem.hasDiscount = true;
        discountedItem.discountedPrice = 30;
        discountedItem.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-1.jpg";
        this.savedItems.push(discountedItem);
        // Item where the size and colour have been selected
        var setSizeAndColourItem = new Item();
        setSizeAndColourItem.name = "ASOS Embroidered Mini Dress";
        setSizeAndColourItem.price = 109;
        setSizeAndColourItem.sizeAndColourSelected = true;
        setSizeAndColourItem.size = "M";
        setSizeAndColourItem.colour = "Black";
        setSizeAndColourItem.canAddToBag = true;
        setSizeAndColourItem.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523415017/women-product-8.jpg";
        this.savedItems.push(setSizeAndColourItem);
        // Normal item without discount and size and colour not set yet
        var item = new Item();
        item.name = "Lee Sherpa Rider Denim Jacket";
        item.price = 218;
        item.hasDiscount = false;
        item.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-4.jpg";
        item.size = "M";
        item.colour = "Black";
        this.savedItems.push(item);
        this.itemsCount = this.savedItems.length;
    };
    SavedItemsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SavedItemsPage');
    };
    SavedItemsPage.prototype.editItem = function (item) {
        item.isInEditMode = !item.isInEditMode;
    };
    SavedItemsPage.prototype.colourPopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    SavedItemsPage.prototype.sizePopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    SavedItemsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-saved-items',template:/*ion-inline-start:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\saved-items\saved-items.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <ion-title>\n\n      <ion-row>\n\n        <p class="title">SAVED ITEMS</p>\n\n        <p class="items-count">({{ itemsCount }} items)</p>\n\n      </ion-row>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-item no-lines class="saved-item" *ngFor="let item of savedItems">\n\n      <img item-start src="{{ item.picture }}">\n\n\n\n      <ion-col *ngIf="!item.isInEditMode">\n\n        <ion-item class="name-bin" no-lines>\n\n          <p item-start>{{ item.name }}</p>\n\n\n\n          <button item-end ion-button clear icon-only>\n\n            <ion-icon name="ios-trash-outline"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n\n\n        <ion-row *ngIf="!item.hasDiscount">\n\n          <p class="actual-price">${{ item.price }}</p>\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="item.hasDiscount">\n\n          <p class="discounted-price">${{ item.discountedPrice }}</p>\n\n          <p class="non-discounted-price">${{ item.price }}</p>\n\n        </ion-row>\n\n\n\n        <p class="size-colour" *ngIf="item.sizeAndColourSelected">{{ item.size }} / {{ item.colour }}</p>\n\n        <p class="select-size-colour" *ngIf="!item.sizeAndColourSelected">SELECT EDIT FOR SIZE AND COLOUR</p>\n\n\n\n        <ion-item no-lines>\n\n          <button class="edit-button" item-start outline ion-button (click)="editItem(item)">\n\n            <p>EDIT</p>\n\n          </button>\n\n\n\n          <button *ngIf="!item.canAddToBag" class="add-to-bag-button" outline ion-button disabled>\n\n            <p>ADD TO BAG</p>\n\n          </button>\n\n\n\n          <button *ngIf="item.canAddToBag" class="add-to-bag-button" outline ion-button>\n\n            <p>ADD TO BAG</p>\n\n          </button>\n\n        </ion-item>\n\n      </ion-col>\n\n\n\n      <ion-col *ngIf="item.isInEditMode">\n\n        <ion-item no-lines class="colour-size" (click)="colourPopOver($event)">\n\n          <p item-start>COLOUR</p>\n\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n\n        </ion-item>\n\n\n\n        <ion-item no-lines class="colour-size" (click)="sizePopOver($event)">\n\n          <p item-start>SIZE</p>\n\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n\n        </ion-item>\n\n\n\n        <ion-row>\n\n          <ion-col text-center (click)="editItem(item)">\n\n            <button class="cancel-button" ion-button>\n\n              <p>CANCEL</p>\n\n            </button>\n\n          </ion-col>\n\n\n\n          <ion-col text-center (click)="editItem(item)">\n\n            <button class="apply-button" ion-button>\n\n              <p>APPLY</p>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\domin\Documents\acccim-admin\asos-admin\src\pages\saved-items\saved-items.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], SavedItemsPage);
    return SavedItemsPage;
}());

var Item = /** @class */ (function () {
    function Item() {
    }
    return Item;
}());

//# sourceMappingURL=saved-items.js.map

/***/ })

},[234]);
//# sourceMappingURL=main.js.map